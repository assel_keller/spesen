-- MySQL dump 10.13  Distrib 5.5.29, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: DB_SPESEN
-- ------------------------------------------------------
-- Server version	5.5.29-0ubuntu0.12.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `TB_Log`
--

DROP TABLE IF EXISTS `TB_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TB_Log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(1024) DEFAULT NULL,
  `host` varchar(30) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `priority` enum('debug','info','error','critical') DEFAULT 'info',
  `message` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TB_Log`
--

LOCK TABLES `TB_Log` WRITE;
/*!40000 ALTER TABLE `TB_Log` DISABLE KEYS */;
INSERT INTO `TB_Log` VALUES (5,'lukas','localhost','2013-06-16 23:12:53','','asdf'),(6,'lukas','localhost','2013-06-16 23:14:19','info','Eroglreich eingelogt'),(7,'lukas','localhost','2013-06-17 08:35:37','info','Eroglreich eingeloggt'),(8,'lukas','localhost','2013-06-17 08:45:19','info','Eroglreich eingeloggt'),(9,'lukas','localhost','2013-06-17 08:45:26','info','Eroglreich eingeloggt'),(10,NULL,'vmLP1','2013-06-17 09:15:05','info','lukas Passwort gesetzt durch Admin'),(11,'lukas','localhost','2013-06-17 09:16:07','info','Eroglreich eingeloggt'),(12,'lukas','localhost','2013-06-17 09:17:27','info','Eroglreich eingeloggt'),(13,NULL,'vmLP1','2013-06-17 09:17:34','info','lukas Passwort gesetzt durch Admin'),(14,NULL,'vmLP1','2013-06-17 09:24:49','info','testqasdf modifiziert durch Admin'),(15,NULL,'vmLP1','2013-06-17 09:24:54','info','testqasdf modifiziert durch Admin'),(16,NULL,'vmLP1','2013-06-17 09:25:14','info','lukas Passwort gesetzt durch Admin'),(17,'lukas','localhost','2013-06-17 09:26:17','info','Eroglreich eingeloggt'),(18,NULL,'vmLP1','2013-06-17 09:26:28','info','lukas Passwort gesetzt durch Admin'),(19,'lukas','localhost','2013-06-17 09:26:35','info','Eroglreich eingeloggt'),(20,NULL,'vmLP1','2013-06-17 09:26:45','info','lukas Passwort gesetzt durch Admin'),(21,'lukas','localhost','2013-06-17 10:14:48','info','Eroglreich eingeloggt'),(22,NULL,'vmLP1','2013-06-17 10:25:49','info','blub hinzugefÃ¼gt durch Admin'),(23,NULL,'vmLP1','2013-06-17 10:30:26','info','lorem hinzugefÃ¼gt durch Admin'),(24,NULL,'vmLP1','2013-06-17 10:34:16','info','blub gelÃ¶scht durch Admin'),(25,'lukas','localhost','2013-06-17 11:33:00','info','Eroglreich eingeloggt'),(26,'lukas','localhost','2013-06-17 12:43:35','info','Eroglreich eingeloggt'),(27,'lukas','localhost','2013-06-17 12:43:58','info','Eroglreich eingeloggt'),(28,'lukas','localhost','2013-06-17 12:49:36','info','Eroglreich eingeloggt'),(29,'lukas','localhost','2013-06-17 12:50:09','info','Eroglreich eingeloggt'),(30,'lukas','localhost','2013-06-17 15:18:12','info','Eroglreich eingeloggt'),(31,'lukas','localhost','2013-06-17 15:21:43','info','Eroglreich eingeloggt'),(32,'lukas','localhost','2013-06-17 15:22:02','info','Eroglreich eingeloggt'),(33,'lukas','localhost','2013-06-17 15:22:31','info','Eroglreich eingeloggt'),(34,'lukas','localhost','2013-06-17 15:23:08','info','Eroglreich eingeloggt'),(35,'lukas','localhost','2013-06-17 15:23:40','info','Eroglreich eingeloggt'),(36,'lukas','localhost','2013-06-17 15:24:13','info','Eroglreich eingeloggt'),(37,'lukas','localhost','2013-06-17 15:25:56','info','Eroglreich eingeloggt'),(38,'lukas','localhost','2013-06-17 15:26:11','info','Eroglreich eingeloggt'),(39,'lukas','localhost','2013-06-17 16:58:44','info','Eroglreich eingeloggt'),(40,'lukas','localhost','2013-06-17 17:14:00','info','Eroglreich eingeloggt'),(41,'lukas','localhost','2013-06-17 17:14:02','info','Eroglreich eingeloggt'),(42,'lukas','localhost','2013-06-17 17:15:01','info','Eroglreich eingeloggt'),(43,'lukas','localhost','2013-06-17 17:15:57','info','Eroglreich eingeloggt'),(44,'lukas','localhost','2013-06-17 17:17:08','info','Eroglreich eingeloggt'),(45,'lukas','localhost','2013-06-17 17:17:34','info','Eroglreich eingeloggt'),(46,'lukas','localhost','2013-06-17 17:33:23','info','Eroglreich eingeloggt'),(47,'lukas','localhost','2013-06-17 17:35:33','info','Eroglreich eingeloggt'),(48,'lukas','localhost','2013-06-17 17:36:50','info','Eroglreich eingeloggt'),(49,'lukas','localhost','2013-06-17 17:37:10','info','Eroglreich eingeloggt'),(50,'lukas','localhost','2013-06-17 17:37:23','info','Eroglreich eingeloggt'),(51,'lukas','localhost','2013-06-17 17:38:24','info','Eroglreich eingeloggt'),(52,'lukas','localhost','2013-06-17 18:58:40','info','Eroglreich eingeloggt'),(53,'lukas','localhost','2013-06-17 19:00:04','info','Eroglreich eingeloggt'),(54,'lukas','localhost','2013-06-17 19:05:10','info','Eroglreich eingeloggt'),(55,'lukas','localhost','2013-06-17 19:06:02','info','Eroglreich eingeloggt'),(56,'lukas','localhost','2013-06-17 19:19:04','info','Eroglreich eingeloggt'),(57,'lukas','localhost','2013-06-17 19:19:36','info','Eroglreich eingeloggt'),(58,'lukas','localhost','2013-06-17 21:38:19','info','Eroglreich eingeloggt'),(59,NULL,'vmLP1','2013-06-17 23:07:01','info','asdfasdf hinzugefÃ¼gt durch Admin'),(60,'lukas','localhost','2013-06-18 03:30:31','info','Eroglreich eingeloggt'),(61,'lukas','localhost','2013-06-18 07:22:09','info','Eroglreich eingeloggt');
/*!40000 ALTER TABLE `TB_Log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TB_Mitarbeiter`
--

DROP TABLE IF EXISTS `TB_Mitarbeiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TB_Mitarbeiter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Username` varchar(1024) NOT NULL,
  `Vorname` varchar(1024) NOT NULL,
  `Nachname` varchar(1024) NOT NULL,
  `Funktion` varchar(1024) NOT NULL,
  `Password` varchar(40) DEFAULT NULL,
  `Salt` varchar(6) DEFAULT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `lastLogin` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TB_Mitarbeiter`
--

LOCK TABLES `TB_Mitarbeiter` WRITE;
/*!40000 ALTER TABLE `TB_Mitarbeiter` DISABLE KEYS */;
INSERT INTO `TB_Mitarbeiter` VALUES (1,'lukas','lukas','mueller','','c84b8d894734848435e725679acf44fb6e29e867','b785a8','2013-06-18 07:22:09','2013-06-18 07:22:09',1),(2,'testqasdf','test','test','','7e2f76c4e0f92739371a2625a7a0db84e773494f','10979d','2013-06-16 23:06:18','0000-00-00 00:00:00',1),(4,'lorem','lorem','test','','e27704ee77584a23ed03030ba6c5aa492c4b37a2','088312','2013-06-17 10:30:26','0000-00-00 00:00:00',1),(5,'asdfasdf','asfdasdf','asfasdf','','38bc7b82da2814a6af353a6617b40e8a3224bef5','768df1','2013-06-17 23:07:01','0000-00-00 00:00:00',0);
/*!40000 ALTER TABLE `TB_Mitarbeiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TB_Spesenkopf`
--

DROP TABLE IF EXISTS `TB_Spesenkopf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TB_Spesenkopf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Datum` date NOT NULL DEFAULT '0000-00-00',
  `Genehmigung` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Auszahlung` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `MitarbeiterFK` int(11) NOT NULL,
  `GenehmigerFK` int(11) DEFAULT NULL,
  `AuszahlerFK` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `MitarbeiterFK` (`MitarbeiterFK`),
  KEY `GenehmigerFK` (`GenehmigerFK`),
  KEY `AuszahlerFK` (`AuszahlerFK`),
  CONSTRAINT `TB_Spesenkopf_ibfk_1` FOREIGN KEY (`MitarbeiterFK`) REFERENCES `TB_Mitarbeiter` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `TB_Spesenkopf_ibfk_2` FOREIGN KEY (`GenehmigerFK`) REFERENCES `TB_Mitarbeiter` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `TB_Spesenkopf_ibfk_3` FOREIGN KEY (`AuszahlerFK`) REFERENCES `TB_Mitarbeiter` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TB_Spesenkopf`
--

LOCK TABLES `TB_Spesenkopf` WRITE;
/*!40000 ALTER TABLE `TB_Spesenkopf` DISABLE KEYS */;
/*!40000 ALTER TABLE `TB_Spesenkopf` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TB_Spesenposition`
--

DROP TABLE IF EXISTS `TB_Spesenposition`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TB_Spesenposition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Spesenart` varchar(1024) NOT NULL,
  `Betrag` decimal(12,2) DEFAULT NULL,
  `SpesenkopfFK` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `SpesenkopfFK` (`SpesenkopfFK`),
  CONSTRAINT `TB_Spesenposition_ibfk_1` FOREIGN KEY (`SpesenkopfFK`) REFERENCES `TB_Spesenkopf` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TB_Spesenposition`
--

LOCK TABLES `TB_Spesenposition` WRITE;
/*!40000 ALTER TABLE `TB_Spesenposition` DISABLE KEYS */;
/*!40000 ALTER TABLE `TB_Spesenposition` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-18  9:28:34
