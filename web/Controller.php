<?php

class Controller{
	private $html = null;
	
	public function __construct(){
		$this->html = new Html();
	}


	public function runAction($action='index'){
		if($action == 'index'){
			$this->actionIndex();
		}elseif($action == 'login'){
			$this->actionLogin();
		}elseif($action =='profile'){
			if(isset($_SESSION['user']['username'])){
				$this->actionProfile();
			}else{
				$this->actionError('Aufgerufen Action existiert nicht.');
			}
		}elseif($action == 'logout'){
			$this->actionLogout();
		}elseif($action == 'userlist'){
			$this->actionUserList();
		}elseif($action == 'log'){
			$this->actionLog();
		}elseif($action == 'userdelete'){
			if(isset($_POST['userid'])){
				$id = (int)$_POST['userid'];
				$this->actionUserDelete($id);
			}else{
				$this->actionError('Fehlende Paramater.');
			}
		}elseif($action == 'useradd'){
			$this->actionUserAdd();
		}elseif($action == 'userview'){
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$this->actionUserView($id);
			}else{
				$this->actionError('Fehlende Paramater.');
			}
		}elseif($action == 'useredit'){
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$this->actionUserEdit($id);
			}else{
			$this->actionError('Missing paramaters.');
			}
		}elseif($action == 'spesenlist'){
			$this->actionSpesenList();
		}elseif($action == 'genehmigung'){
			$id = (int)$_GET['id'];
			$this->actionGenehmigung($id);
		}elseif($action == 'auszahlung'){
			$id = (int)$_GET['id'];
			$this->actionAuszahlung($id);
		}elseif($action == 'spesenview'){
			$id = (int)$_GET['id'];
			$this->actionSpesenView($id, $message =null);
		}else{
			$this->actionError('Requested action does not exist.');
		}
	}
	
	

	private function actionSpesenList(){
		$tbl = array(array('Mitarbeiter','Datum', 'totaler Betrag',''));

		if(isset($_POST['submitFilter'])){
			$datum = $this->escape($_POST['date']);

			$MitarbeiterID = $this->escape($_POST['MA']);
			$Mitarbeiter = User::model()->findByPk($MitarbeiterID);

			// user und datum gesetzt!!
			if($datum != NULL && !empty($datum) && $Mitarbeiter != NULL){
				$spesenkopfe = Spesenkopf::model()->findAllByAttributes(array('Datum' => $datum, 'MitarbeiterFK' => $Mitarbeiter->id));
			// user gesetzt aber datum nicht
			}elseif(($datum == NULL || empty($datum)) && $Mitarbeiter != NULL){
				$spesenkopfe = Spesenkopf::model()->findAllByAttributes(array('MitarbeiterFK' => $Mitarbeiter->id));
			// datum gesetzt aber user nicht
			}elseif($datum != NULL && !empty($datum) && $Mitarbeiter == NULL){
				$spesenkopfe = Spesenkopf::model()->findAllByAttributes(array('Datum' => $datum));
			}else{
				$spesenkopfe = Spesenkopf::model()->findAll();
			}
		}else{
			$spesenkopfe = Spesenkopf::model()->findAll();
		}


		if($spesenkopfe != 0 && !empty($spesenkopfe)){
			foreach($spesenkopfe as $sk){
				$td_arr = array();
				$user = User::model()->findByPk($sk->MitarbeiterFK);
				$td_arr[] = $user->Vorname .' '. $user->Nachname;
				$td_arr[] = $sk->Datum;

				$spesenposi = Spesenposition::model()->findAllByAttributes(array('SpesenkopfFK' => $sk->id));

				$betrag = 0;
				if($spesenposi != 0 && !empty($spesenposi)){
					foreach($spesenposi as $sp){
						$betrag = $betrag + $sp->Betrag;
					}
				}
				$td_arr[] = $betrag;
				$td_arr[] = '<a href="index.php?action=spesenview&id='.$sk->id.'"><img src="./images/detail.png" alt="Detailansicht" /> </a>';
				$tbl[] = $td_arr;
			}
			$spesentable = $this->html->createTable($tbl, '','');
		}else{
				$spesentable = 'keine Spesen gefunden';
		}

		$options = '';
		$users = User::model()->findAllByOrder(array('username'=>'ASC'));
		foreach($users as $user){
			$options .= '<option value="'.$user->id.'">'.$user->Vorname .' '.$user->Nachname.'</option>';
		}
				
		$sortsearch= '
		<form action="index.php?action=spesenlist" method="POST">
			Mitarbeiter:
			<select id="MA" name="MA">
			<option> </option>
			'.$options.'
			</select>
			<br />Datum:
			<input type="text" id="datepicker" name="date" />
			<input type="submit" name="submitFilter" value="Auswahl eingrenzen" />
		</form>
		';

		
		$content = $this->html->getPage('Spesenlist');
		$content = str_replace('{{TABELLE}}', $spesentable, $content);
		$content = str_replace('{{SORT_SEARCH}}', $sortsearch, $content);
		echo $this->html->buildPage('Spesen verwalten', $content);
	}



	private function actionIndex($message = NULL){
		if(isset($_SESSION['user']['username']) ){
			$user = User::model()->findByAttributes(array('Username' => $_SESSION['user']['username'] ));
			$actionfield = '';
			$spesen = '';
			$output = '';
			$v = new Validator();

			if(isset($_POST['submitSpesen'])){
				$datum = $this->escape($_POST['date']);
				if($datum != NULL && $datum != ''){
					$i= 1;
					do{
						$art = '';
						$betrag = 0;

						if(isset($_POST['art'.$i.'']))
							$art = $this->escape($_POST['art'.$i]);
						if(isset($_POST['bet'.$i])){
							$betrag = $this->escape($_POST['bet'.$i]);
							if(!$v->isFrankenBetrag($betrag)){
								$this->actionIndex('<div class="flash-error">Spesen konnten nicht gespeichert werden, Betrag ungültig!</div>');
							}
						}

						if($i == 1 && $betrag == 0){
							$this->actionIndex('<div class="flash-error">Spesen konnten nicht gespeichert werden, Betrag ungültig!</div>');
						}

						$spesenposition[] = array($art, $betrag);
						$i++;
					}while($art != '' && $betrag != 0);
					

					for($j=0; $j < (count($spesenposition)-1); $j++){
						if(!$v->isFrankenBetrag($spesenposition[$j][1])){
							$this->actionIndex('<div class="flash-error">Spesen konnten nicht gespeichert werden, Betrag ungültig!</div>');
						}
					}

					$sk = new Spesenkopf();
					$arg = array('Datum' => $datum ,'MitarbeiterFK' =>$user->id);
					$sk->populate($arg);
					$sk->save();
					
					for($j=0; $j < (count($spesenposition)-1); $j++){
						$sp = new Spesenposition();
						$arg = array('Spesenart' => $spesenposition[$j][0], 'Betrag' => $spesenposition[$j][1] , 'SpesenkopfFK' => $sk->id);
						$sp->populate($arg);
						$sp->save();
						$message = '<div class="flash-success">Spesen erfolgreich gespeichert.</div>';
					}
				}else{
					$message = '<div class="flash-error">Datum ungültig.</div>';
				}
			}

			$userstring = $user->Vorname . ' ' . $user->Nachname;
			$content = $this->html->getPage('home');
			$search = array('{{MESSAGE}}', '{{ACTION_FIELD}}', '{{SPESEN}}', '{{USER}}');
			$replace = array($message, $actionfield, $spesen, $userstring, $output);
			$content = str_replace($search, $replace, $content);
			echo $this->html->buildPage('Home', $content);

		}else{
			$this->actionLogin();
		}
	}
	
	private function actionSpesenView($id, $message){
		$sk = Spesenkopf::model()->findByPk($id);
		$Mitarbeiter =User::model()->findByPk($sk->MitarbeiterFK);
		$output1 = '
		<table>
		<tr>
			<td>Mitarbeiter:</td>
			<td>'. $Mitarbeiter->Vorname .' '. $Mitarbeiter->Nachname.'</td>
		</tr>
		<tr>
		<td>Datum:</td>
		<td>'.$sk->Datum.'</td>
		</tr>';
		

		if($sk->AuszahlerFK != NULL){
			$Auszahler =User::model()->findByPk($sk->AuszahlerFK);
			$output1 .= '<tr><td>Auszahlung</td><td>'.$sk->Auszahlung.'</td></tr>';
			$output1 .= '<tr><td>Auszahler</td><td>'.$Auszahler->Vorname. ' '.$Auszahler->Nachname. '</td></tr>';
		}
		
		if($sk->GenehmigerFK != NULL){
			$Genehmiger =User::model()->findByPk($sk->GenehmigerFK);
			$output1 .= '<tr><td>Genehmigung</td><td>'.$sk->Genehmigung.'</td></tr>';
			$output1 .= '<tr><td>Genehmigung</td><td>'.$Genehmiger->Vorname. ' '.$Genehmiger->Nachname. '</td></tr>';
		}
		
		$output1 .='</table>';

		$tbl = array(array('Art','Betrag'));
		$spesenpositionen = Spesenposition::model()->findAllByAttributes(array('SpesenkopfFK' => $sk->id));
			if($spesenpositionen != 0){
				foreach($spesenpositionen as $sp){
					$tbl[] = array($sp->Spesenart, $sp->Betrag);
				}
			}
		$output2 = $this->html->createTable($tbl, '','');


		if($sk->GenehmigerFK == NULL){
			$output2 .= '<a href="index.php?action=genehmigung&id='.$sk->id.'"><img src="./images/check.png" alt="genehmigen" /> </a>';
		}

		if($sk->AuszahlerFK == NULL){
			$output2 .= '<a href="index.php?action=auszahlung&id='.$sk->id.'"><img src="./images/buy.png" alt="genehmigen" /> </a>';
		}
		// action icons
		$content = $this->html->getPage('spesenview');
		$search = array('{{MESSAGE}}', '{{TB1}}', '{{TB2}}');
		$replace = array($message, $output1, $output2, $content);
		$content = str_replace($search, $replace, $content);
		echo $this->html->buildPage('Detailansicht Spesen', $content);

	}

	private function actionGenehmigung($id){
		$message = '';
		$sk = Spesenkopf::model()->findByPk($id);
		if($sk == null)
			$this->actionError('Spesenkopf existiert nicht');
		$user = User::model()->findByAttributes(array('Username' => $_SESSION['user']['username'] ));
		$sk->GenehmigerFK = $user->id;
		$sk->Genehmigung = date("Y-m-d H:i:s", time());
		$sk->save();
		$message = '<div class="flash-success">Spesenkopf erfolgreich genehmigt!</div>';
		$this->actionSpesenView($sk->id, $message);
	}
	
	private function actionAuszahlung($id){
		$message = '';
		$sk = Spesenkopf::model()->findByPk($id);
		if($sk == null)
			$this->actionError('Spesenkopf existiert nicht');
		$user = User::model()->findByAttributes(array('Username' => $_SESSION['user']['username'] ));
		$sk->AuszahlerFK = $user->id;
		$sk->Auszahlung = date("Y-m-d H:i:s", time());
		$sk->save();
		$message = '<div class="flash-success">Spesenkopf erfolgreich ausgezahlt!</div>';
		$this->actionSpesenView($sk->id, $message);
	}

	public function actionError($error){
		$content = $this->html->getPage('error');
		$content = str_replace('{{DETAIL_MSG}}', $error, $content);
		echo $this->html->buildPage('Error', $content, 'error');
		die();
	}

/**
 * Function: actionUserDelete
 *
 * Description:
 * Deletes an user and set the according log.
 *
 * @param $id (int) The users id
 */
	private function actionUserDelete($id){
		$user = User::model()->findByPk($id);
		if($user == null)
			$this->actionError('User existiert nicht.');

		$message ='';
		$username = $user->Username;

		if($user->delete()){
			$message = '<div class="flash-success">User erfolgreich gelölscht.</div>';
			$l = new Logger();
			$l->info($username . ' gelöscht durch Admin');
		}else{
			$this->actionError('Fehler beim löschen des Users.');
		}
		$this->actionUserList($message);
	}

	/**
	 * Function: actionUserEdit
	 *
	 * Description:
	 * Creates the page to edit an user and save changes to an user in the db.
	 *
	 * @param $id (int) the id of the user
	 */
	private function actionUserEdit($id, $message = ''){
		$conf = Config::getInstance();
		$max_password_length = $conf->max_password_length;
		$user = User::model()->findByPk($id);
		if($user == null)
			$this->actionError('User existiert nicht.');

		// save password changes
		if(isset($_POST['submitPassword'])){
			$message = $this->changePassword($user);
			$l = new Logger();
			$l->info( $user->Username . ' Passwort gesetzt durch Admin');
		// save Permission changes
		}elseif(isset($_POST['submitInfo'])){
			$v = new Validator();
			$message = '';

			// get post data
			$username = $this->escape($_POST['username']);
			$surname = $this->escape($_POST['surName']);
			$givenname = $this->escape($_POST['givenName']);
			$funktion = $this->escape($_POST['funktion']);

			$admin = false;
			if(isset($_POST['admin'])){
				$admin = $_POST['admin'];
				if($admin == 'on'){
					$admin = true;
				}else{
					$admin = false;
				}
			}
			
			// check if all required fields were filled
			if($username == NULL || $surname == NULL || $givenname == NULL){
				$message = 'Please fill all required fields';
			}else{
				// valite given informations and set message if invalid
				if(!$v->isString($username, 1, 1024))
					$message .= 'Username ungültig! <br />';
				if(!$v->isString($surname, 2, 1024))
					$message .= 'Vorname ungültig! <br />';
				if(!$v->isString($givenname, 2, 1024))
					$message .= 'Nachname ungültig! <br />';
				if($funktion != NULL && !$v->isString($funktion, 1, 1024))
					$message .= 'Funktion ungültig! <br />';
			}
			// display a message if the username is already in use
			if($user->Username != $username){
				if(User::model()->findByAttributes(array('Username' => $username )))
					$message .= 'Username wird bereits verwendet, der alte User wurde überschrieben!';
			}

			// if not everything was fine display the error message
			if($message != ''){
				$message = '<div class="flash-error">User wurde nicht gespeichert:  &nbsp;' . $message. ' </div>';
			}else{
				// if everything was fine, save the changes to the db
				$arg = array('Username' => $username, 'Vorname' => $surname, 'Nachname' => $givenname, 'Funktion'=> $funktion, 'isAdmin' => $admin);
				$user->populate($arg);
				$user->save();
				$message = '<div class="flash-success">User erfolgreich gespeichert ' . $surname . ' ' . $givenname . ' : ' . $username . '</div>';
				$l = new Logger();
				$l->info( $user->Username . ' modifiziert durch Admin');
			}
		}

		//build page
		$content = $this->html->getPage('useredit');
		$user_string = $user->Vorname . ' ' . $user->Nachname . ' (' . $user->Username . ')';
		$search = array('{{EDIT_USER}}', '{{EDIT_USER_ID}}', '{{SAVE_MSG}}', '{{MAX_PW_LENGTH}}');
		$replace = array($user_string, $user->id, $message, $max_password_length);
		$content = str_replace($search, $replace, $content);
		
		$checked = '';
		if($user->isAdmin)
			$checked = 'checked';
		else
			$checked = '';
			
		$srch = array('{{EDIT_USER_ID}}', '{{USERNAME}}', '{{surName}}', '{{givenName}}', '{{funktion}}', '{{checked}}');
		$rplc = array($user->id, $user->Username, $user->Vorname, $user->Nachname, $user->Funktion, $checked);
		$content = str_replace($srch, $rplc, $content);

		echo $this->html->buildPage('User bearbeiten', $content, 'useredit');
	}

	/**
	 * Function: changePassword
	 *
	 * Description:
	 * Validates the password and sets it in the db if valid.
	 *
	 * @param $user (object) The user
	 *
	 * @return $mesasge (string) error/success message
	 */
	private function changePassword($user){
		$conf = Config::getInstance();
		$max_password_length = $conf->max_password_length;
		$min_password_length = $conf->min_password_length;
		$v = new Validator();
		$pw1 = $_POST['pw1'];
		$pw2 = $_POST['pw2'];

		//$min_password_length
		//$max_password_length

		if(!$v->isString($pw1, $min_password_length, $max_password_length)){
			$message = '<div class="flash-error">Ungültiges Passwort! 
						Passwortlänge muss zwischen '.
						$min_password_length .' und '. $max_password_length .' sein!
						</div>';
		}elseif($pw1 == $pw2){
			$user->password = $pw1;
			$user->save();
			$message = '<div class="flash-success">Passwort erfolgreich geändert!</div>';
		}else{
			$message = '<div class="flash-error">Passwort und wiederholtes Passwort stimmen nicht überein.</div>';
		}
		return $message;
	}


private function actionProfile(){
		$message = '';
		// aktueller User auslesen
		$user = User::model()->findByAttributes(array('username' => $_SESSION['user']['username'] ));
		
		// Passwort ändern funktionalität
		if(isset($_POST['submitPW'])){
			//post data auslesen
			$oldPW = $this->escape($_POST['oldPW']);
			$pw1 = $this->escape($_POST['pw1']);
			$pw2 = $this->escape($_POST['pw2']);

			$v = new Validator();
			// altes pw prüfen
			$query = new Query('function', 'check_password');
			$query->addParameters(array($user->Username, $oldPW));
			$storage = call_user_func(array('Mysql', 'getInstance'), array());
			if($storage->query($query) == 0){
				$message .= 'Altes Passwort stimmt nicht! <br />';
			}else{
				// validate new pw
				if(!$v->isString($pw1, 6, 1024))
					$message .= 'Ungültiges Password! Bitte beachten Sie, dass das Password mindestens 6 Zeichen lang sein muss!<br />';
				if($pw1 != $pw2)
					$message .= 'Passwörter stimmen nicht überein!<br />';
				
				// alles ok
				if($message != ''){
					$message ='<div class="flash-error">'.$message.'</div>';
				}else{	
					// pw setzten und speichern
					$user->Password = $pw1;
					$user->save();
					$message ='<div class="flash-success">Passwort erfolgreich geändert</div>';
				}
			}
		}

		$content = $this->html->getPage('profile');
		$content = str_replace('{{MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Profil', $content);
	}

 /**
	 * Function: actionAdmin
	 *
	 * Description:
	 * Creates the Page which displays the list with all users.
	 *
	 * @param $saveMsg (string) The message which is displayed if an user was deleted.
	 */
	private function actionUserList($saveMsg = ''){
		$users = User::model()->findAllByOrder(array('username'=>'ASC'));

		// array which holds the data for the table, first row = tbl head
		$tbl = array(
				array('Username','Name', 'Vorname', 'Funktion', 'Erstelldatum', 'letztes Login', 'admin', '','','')
		);

		$i=0;
		foreach($users as $user){
			//create the row for the usr and add all columns to the row
			$row = array();
			$row[] = $user->Username;
			$row[] = $user->Vorname;
			$row[] = $user->Nachname;
			$row[] = $user->Funktion;
			
			// erstelldatum
			if(strtotime($user->create_date) < 0 || strtotime($user->create_date) === false){
				$row[] = 'Nie';
			}else{
				$row[] = date('H:i:s d.m.Y', strtotime($user->create_date));
			}
			
			// letztes Login
			if(strtotime($user->lastLogin) < 0 || strtotime($user->lastLogin) === false){
				$row[] = 'Nie';
			}else{
				$row[] = date('H:i:s d.m.Y', strtotime($user->lastLogin));
			}
			
			// is admin
			if($user->isAdmin == 1){
				$row[] = 'Ja';
			}else{
				$row[] = 'Nein';
			}
			
			// edit etc icons!!!
			$row[]  = '<a href="index.php?action=userview&id=' . $user->id . '"><img src="./images/detail.png" /></a>';
			$row[]  = '<a href="index.php?action=useredit&id=' . $user->id . '"><img src="./images/edit.png" /></a>';
			$user_string  = $user->Vorname . ' ' . $user->Nachname ;
			$row[]  = '<a href="#" onclick="deleteUser(this, ' . $user->id . ', \'' . $user_string . '\')"><img src="./images/delete.png" /></a>';
			// add the row to the array which holds the informations for the table
			$tbl[] = $row;
		}
		// create the table
		$user_table = $this->html->createTable($tbl, '', 'usertable');
		// build the page
		$content = $this->html->getPage('userlist');
		$content = str_replace(array('{{SAVE_MSG}}', '{{USER_TABLE}}'), array($saveMsg, $user_table), $content);
		echo $this->html->buildPage('User verwalten', $content, 'UserList');
	}
 
 	/**
	 * Function: createUserLog
	 *
	 * Description:
	 * This function gets all logs of the given user out of the db.
	 *
	 * @param $user (object) The user.
	 * @return $user_log (string) All logs from the user in a table.
	 */
	private function createUserLog($user){
		$tbl = array(
			array('Aktivität', 'Datum'),
			array('last Login',$user->lastLogin),
		);

		$logs = Log::model()->findAllByAttributes(array('user' => $user->Username));
		if($logs != NULL){
			foreach($logs as $log){
				$tbl[] = array($log->message, $log->time);
			}
		}

		$user_log = $this->html->createTable($tbl, 'center noFilter');
		return $user_log;
	}


	/**
	 * Function: actionUserView
	 *
	 * Description:
	 * Creates the page which displays detailed information about an user.
	 *
	 * @param $id (int) The users id.
	 */
	private function actionUserView($id){
		$user = User::model()->findByPk($id);
		if($user == null)
			$this->actionError('User existiert nicht');

		$user_string = $user->Vorname . ' ' . $user->Nachname . ' (' . $user->Username .')';
		// build the table with the informations
		$arr = array(array('Eigenschaft&nbsp;  &nbsp; ','Wert'));
		$arr[] = array('Vorname', $user->Vorname);
		$arr[] = array('Nachname', $user->Nachname);
		$arr[] = array('Funktion', $user->Funktion);
		$row[] = array('Erstelldatum',date('H:i:s d.m.Y', strtotime($user->create_date)));
		
		if(strtotime($user->lastLogin) < 0 || strtotime($user->lastLogin) === false){
			$row[] = 'Nie';
		}else{
			$row[] = date('H:i:s d.m.Y', strtotime($user->lastLogin));
		}
		$arr[] = array('Admin', (($user->isAdmin) ? 'Ja' : 'Nein'));

		// create the table with the informations
		$user_table = $this->html->createTable($arr, 'center');
		// create the table with the logs
		$user_log = $this->createUserLog($user);

		// build and display the the page
		$content = $this->html->getPage('userview');
		$user_string .= '  <a href="index.php?action=useredit&id=' . $user->id . '"><img src="./images/edit.png" /></a>';
		$search = array('{{VIEW_USER}}', '{{USER_LOG}}', '{{USER_INFOS}}' );
		$replace = array($user_string, $user_log, $user_table);
		$content = str_replace($search, $replace, $content);
		echo $this->html->buildPage('Detailansicht User', $content, 'userview');
	}



 	/**
	 * Function: actionLog
	 *
	 * Description:
	 * Creates the page which displays all logs.
	 */
	private function actionLog(){
		// the array in which the informations for the table are hold
		$arr = array(array('User','Host', 'Time','Priority', 'Message'));

		// get all logs
		$logs = Log::model()->findAllByOrder(array('time'=>'DESC'));
		if($logs != NULL){
			foreach($logs as $log){
				// add an row for every log the the array which holds the informations for the table
				$td_arr = array();
				$td_arr[] =  $log->user;
				$td_arr[] = $log->host ;
				if(strtotime($log->time) < 0 || strtotime($log->time) === false){
					$td_arr[] = 'Never';
				}else{
					$td_arr[] =  date('H:i:s d.m.Y', strtotime($log->time)) ;
				}
				$td_arr[] = $log->priority;
				$td_arr[] = $log->message;
				$arr[] = $td_arr;
			}
			// create the table
			$log_table = $this->html->createTable($arr, 'center', 'logtable');
		}else{
			$log_table = 'Keine Logs vorhanden';
		}
		// build the page
		$content = $this->html->getPage('log');
		$content = str_replace('{{LOG_TABLE}}', $log_table, $content);
		echo $this->html->buildPage('Alle Logs', $content, 'log');
	}

	private function actionLogin(){
		$message = '';

		if(isset($_POST['submitLogin'])){
			//post data vorhanden?
			if(!isset($_POST['username']) || !isset($_POST['passwort'])){
				$message .= 'Bitte geben Sie ihren Usernamen und ihr Passwort ein!';
			}else{
				//post data auslesen
				$username = $this->escape($_POST['username']);
				$pw = $this->escape($_POST['passwort']);				
				$user = User::model()->findByAttributes(array('Username' => $username ));
				// existiert der User in der Datenbank?
				if($user !== null){
					// passwort prüfen 
					$query = new Query('function', 'check_password');
					$query->addParameters(array($username, $pw));
					$storage = call_user_func(array('Mysql', 'getInstance'), array());
					if($storage->query($query) == 0){
						$message .= 'Benutzername oder Kennwort falsch';
					}else{
						$_SESSION['user']['username'] = $username;
						if($user->isAdmin == 1){
							$_SESSION['user']['isAdmin'] = true;
						}
						$message .= '<div class="flash-success">Erfolgreich eingeloggt<br /></div>';
						$this->actionIndex($message);
						return;
					}
				}else{
					$message .= 'Username existiert nicht.<br /> Falls Sie noch kein Benutzerkonto besitzten, dann registrieren Sie sich <a href="">hier.</a>';
				}
			}
		}

		if($message !=''){
			$message = '<div class="flash-error">' . $message. ' </div>';
		}else{
			$message = '<div class="flash-notice"> 
						Melden Sie sich hier mit ihrem Benutzerkonto an. <br />
						Sie haben noch kein Konto? Dann registrieren Sie sich 
						<a href="index.php?action=register"> hier</a>. </div>';
		}

		$content = $this->html->getPage('login');
		$content = str_replace('{{MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Login', $content);
	}

	public function actionLogout(){
		if(isset($_SESSION['user']['username']) ){
			session_unset($_SESSION['user']['username']);
			$message = '<div class="flash-success">Erfolgreich ausgeloggt<br /></div>';
			$this->actionIndex($message);
			return;
		}		
	}

	/**
	 * Function:  actionUserAdd
	 *
	 * Description:
	 * Adds an user if all information were valid or displays
	 * the according error message.
	 */
	private function actionUserAdd(){
		$search_values = array('{{val_username}}', '{{val_surName}}', '{{val_givenName}}', '{{val_funktion}}', '{{checked}}');
		$message = '';
		$user = '';
		
		if(isset($_POST['submitUser'])){
			$user = new User();
			$v = new Validator();
			$message = '';

			// read out the post data and escape them
			$username = $this->escape($_POST['username']);
			$surname = $this->escape($_POST['surName']);
			$givenname = $this->escape($_POST['givenName']);
			$funktion = $this->escape($_POST['funktion']);
			
			$admin = false;
			if(isset($_POST['admin'])){
				$admin = $_POST['admin'];
				if($admin == 'on'){
					$admin = true;
				}else{
					$admin = false;
				}
			}

			// check if all necessary field were filled
			if($username === NULL || $surname === NULL || $givenname === NULL ){
				$message = 'Please fill all required fields';
			}else{
				/** validate all informations (if given)
				 * if invalid informations were given, set according message
				 */
				if(!$v->isString($username, 1, 1024))
					$message .= 'Username ungültig!<br />';
				if(!$v->isString($surname, 2, 1024))
					$message .= 'Vorname ungültig <br />';
				if(!$v->isString($givenname, 2, 1024))
					$message .= 'Nachname ungültig <br />';
				if($funktion != NULL && !$v->isString($funktion, 1, 1024))
					$message .= 'Funktion ungültig <br />';
			}

			//display a message if the username is already used
			// but overwrite the user
			if(User::model()->findByAttributes(array('Username' => $username )))
					$message .= 'Username bereits vergeben ( vorhander User wird nun überschrieben)';

			// display the error message if not everything was fine
			if($message != ''){
				$message = '<div class="flash-error">' . $message. ' </div>';
				$content = $this->html->getPage('useradd');
				$content = str_replace('{{SAVE_MSG}}', $message, $content);
				$checked = '';
				if($admin)
					$checked = 'checked';
				$replace_values = array($username, $surname, $givenname, $funktion, $checked);
				$content = str_replace($search_values, $replace_values, $content);
				echo $this->html->buildPage('neuen User hinzufügen', $content, 'useradd');
				return;
			}

			// save the user and set a log
			$arg = array('Username' => $username, 'Vorname' => $surname, 'Nachname' => $givenname, 'isAdmin' => $admin);
			$user->populate($arg);
			$user->save();
			// read out of config and safe...
			$conf = Config::getInstance();
			$user->Password =$conf->default_password;
			$user->save();
			$l = new Logger();
			$l->info($user->Username . ' hinzugefügt durch Admin');
			$message = '<div style="max-width: 1200px; word-wrap: break-word;" class="flash-success"><p>User ' . $surname . ' ' . $givenname . ' : ' . $username . ' erfolgreich hinzugefügt (Achtung Standart Passwort gesetzt!)</div>';
		}
		// build the useradd-page and display the message
		$content = $this->html->getPage('useradd');
		$content = str_replace('{{SAVE_MSG}}', $message, $content);
		$content = str_replace($search_values, '', $content);
		echo $this->html->buildPage('neuen User hinzufügen', $content, 'useradd');
	}


	private function escape($input){
		$escaped = htmlspecialchars(trim($input));
		return $escaped;
	}

}
?>
