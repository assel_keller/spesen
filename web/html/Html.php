<?php

class Html{
	private $layout = 'html/layout.html';
	private $basePath = 'html/';
	
	public function buildPage($site, $content){
		$userinfo = '';		
		if(isset($_SESSION['user']['username'])){
			$userinfo = 'Eingeloggt als: <a href="index.php?action=profile">' . $_SESSION['user']['username'] .'</a>';	
			$userinfo .= '<form action="index.php?action=logout" method="POST"><input type="submit" value="Logout" name="logout" /></form>';
		}


		$html = file_get_contents($this->layout);
		$search = array('{{USERNAME}}',
						'{{SITE}}',
						'{{NAVIGATION}}',
						'{{CONTENT}}',
					);
		
		$replace = array($userinfo,
						$site,
						$this->buildNavigation(),
						$content,
					);
		
		return str_replace($search, $replace, $html);
	}
	
	public function getPage($page){
		return file_get_contents($this->basePath . $page . '.html');
	}

	private function buildNavigation(){
		$nav = '<td><a href="index.php?action=index">Home</a></td>';

		if(isset($_SESSION['user']['isAdmin']) && $_SESSION['user']['isAdmin'] == true){
			$nav .= '<td><a href="index.php?action=spesenlist">Spesen</a></td>';
			$nav .= '<td><a href="index.php?action=userlist">User verwalten</a></td>';
			$nav .= '<td><a href="index.php?action=log">Logs</a></td>';
		}
		return $nav;
	}
	
	/**
	 * Function createTable
	 *
	 * Description:
	 * Creates a table.
	 *
	 * @param $tbl (String-Array) the content of the table
	 * @param $class optional css class for the table
	 * @param $id optional css id for the table
	 *
	 * @return $table (string) the created table
	 */
	public function createTable($tbl, $class = NULL, $id = NULL){
		$table = '<table';

		// dont' create an invalid table if no tbl content is given
		if(empty($tbl) || $tbl == NULL)
			return "";

		// set class and id if given
		if($class !== NULL)
			$table .= ' class="'.$class.'"' ;
		if($id !== NULL)
			$table .= ' id="'.$id.'"' ;
		$table .='> <thead>';

		$i = 0;
		foreach($tbl as $row){
			if($i == 0){
				foreach($row as $head ){
					$table .= '<th>'. $head .'</th>';
				}
				$table .= '</thead><tbody>';
			}else{
				$table .= ($i%2==0) ? '<tr class="even">' : '<tr class="odd">';

				foreach($row as $field ){
					$table .= '<td>'. $field .'</td>';
				}
				$table .= '</tr>';
			}
			$i++;
		}
		$table .= '</tbody></table>';
		return $table;
	}
	
}

?>
